angular.module('list',['ui.router', 'list']);

angular.module('list').config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('list', {
            url: '/list',
            template: '<is-list></is-list>'
        })
        .state('list_add', {
            url: '/list/add',
            template: '<is-list-add></is-list-add>'
        })
        .state('list_edit', {
            url: '/list/edit/:itemId',
            template: '<is-list-edit></is-list-edit>'
        });

    $urlRouterProvider.otherwise('/list');

}]);

angular.module('list').directive('isList', function() {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'list.tpl.html',
        controller: ['$scope', 'listStorage', function($scope, listStorage) {

            $scope.ls = [];

            $scope.reload_items = function() {
                listStorage.loadList().then(function(list) {
                    $scope.ls = list;
                });
            };

            $scope.reload_items();

            $scope.remove_item = function(id) {

                if(confirm('Are you sure that you want to remove this item?')) {

                    listStorage.removeItem(id).then(function() {

                        //reload list if successfully removed

                        listStorage.loadList().then(function(list) { $scope.ls = list;});

                    }).catch(function(err) {
                        alert('Error: ' + err);
                    });

                }
            };
        }]
    };
});

angular.module('list').directive('isListAdd', function() {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'list-add.tpl.html',
        controller: ['$scope', 'listStorage', '$location', function($scope, listStorage, $location) {
            $scope.test = 'test message add';
            $scope.new_item_name = '';

            $scope.submit_form = function() {

                listStorage.addItem($scope.new_item_name).then(function() {

                    $location.path('#/list');

                });
            };

            $scope.go_back = function() {
              if($scope.add_item_form.$dirty && $scope.new_item_name) {
                  if(confirm('You have unsaved data. Are you sure that you want to live this page?')) {
                      $location.path('#/list');
                  }
              }
              else {
                  $location.path('#/list');
              }
            };
        }]
    };
});

angular.module('list').directive('isListEdit', function() {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'list-edit.tpl.html',
        controller: ['$scope', '$stateParams', 'listStorage', '$location', function($scope, $stateParams, listStorage, $location) {

            var itemId = $stateParams.itemId;

            if(!itemId) {
                alert('Item Id is not found!');
                $location.path('#/list');
            }

            $scope.item = {};

            listStorage.getItem(itemId)
                .then(function(item) {
                    $scope.item = item;
                })
                .catch(function(err) {
                    alert('Error: ' + err);
                    $location.path('#/list');
                });

            $scope.submit_form = function() {

                listStorage.saveItem($scope.item)
                    .then(function() {
                        $location.path('#/list');
                    })
                    .catch(function(err) {
                        alert('Error: ' + err);
                    });

            };

            $scope.go_back = function() {

                if($scope.edit_item_form.$dirty && $scope.item.name) {
                    if(confirm('You have unsaved data. Are you sure that you want to live this page?')) {
                        $location.path('#/list');
                    }
                }
                else {
                    $location.path('#/list');
                }
            };
        }]
    };
});

angular.module('list').service('listStorage', [ '$q', function ($q) {

    this.getItem = function(id) {

        return $q(function(resolve, reject) {

            if (!id) reject('Item Id is not provided');

            var name = localStorage[id];

            if(!name) reject('Item is not found');

            resolve({id: id, name: name});
        });
    };

    this.addItem = function(name) {

        return $q(function(resolve, reject) {

            if(!name) reject('Item\'s name was not provided.');

            var ni = {name: name};

            ni.id = guid();

            localStorage.setItem(ni.id, ni.name);

            if(localStorage[ni.id] !== ni.name) reject('Cannot save new item.'); //check if saved successfully

            resolve(true);
        });

    };

    this.saveItem = function(item) {

        return $q(function(resolve, reject) {

            if (!item.id || !item.name) reject('Invalid item');

            localStorage.setItem(item.id, item.name);

            if(localStorage[item.id] !== item.name) reject('Cannot save the item.');  //check if saved successfully

            resolve(true);

        });

    };

    this.loadList = function() {

        return $q(function(resolve, reject) {

            var list = [];

            var keys = Object.keys(localStorage);

            for(var i=0, ll=keys.length; i<ll; i++) {
                var name = localStorage[keys[i]];

                list.push( { id: keys[i], name: name } );
            }

            resolve(list);

        });

    };

    this.removeItem = function(id) {

        return $q(function(resolve, reject) {

            if (!id) reject('Id was not provided');

            localStorage.removeItem(id);

            if (localStorage[id]) reject('Cannot remove the item.');  //check if item still exists

            resolve(true);

        });
    };

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

}]);
