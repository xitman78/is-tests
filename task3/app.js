(function() {

    var module = angular.module('app',[]);

    module.directive('amcTable', function() {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: 'table.tpl.html'
        };
    });

    module.directive('amcGraph', function() {
        return {
            restrict: 'E',
            scope: false,
            templateUrl: 'graph.tpl.html'
        };
    });

    module.controller('MainCtrl', ['$scope', function($scope) {

        $scope.active_tab = 1;

        $scope.table_one = [];
        $scope.table_two = [];

        function generate_table1() {

            // Not sure that I've got right how this generator should work.

            $scope.table_one = [];

            for(var i=1; i<=10; i++) {
                $scope.table_one.push( {x: (Math.round(Math.random() * 9) + 1), y: (Math.round(Math.random() * 9) + 1) } );
            }
        }

        function generate_table2() {

            // Also here, not really understood how this generator should work.

            $scope.table_two = [];

            var unique_map = {};

            for(var i=2; i<=10; i += 2) {

                do {

                    var x = 2 + (Math.round(Math.random() * 8));

                } while( (x % 2 !== 0) || unique_map[x]);

                unique_map[x] = true;

                $scope.table_two.push( {x: x, y:  (Math.round(Math.random() * 9) + 1) } );
            }
        }

        $scope.generate = function() {
            generate_table1();
            generate_table2();
        };

        $scope.generate();

    }]);

}());